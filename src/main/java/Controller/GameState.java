/**

 The abstract class GameState serves as a blueprint for specific game states.
 It defines the common methods that each game state must implement, such as initialization, updating,
 rendering, handling key presses, and handling key releases.
 */
package Controller;

public abstract class GameState {

    protected GameStateController gsc;

    /**
     * Initializes the game state.
     */
    public abstract void init();

    /**
     * Updates the game state.
     */
    public abstract void update();

    /**
     * Renders the game state.
     *
     * @param g the Graphics2D object to render with
     */
    public abstract void render(java.awt.Graphics2D g);

    /**
     * Handles key press events.
     *
     * @param k the key code of the pressed key
     */
    public abstract void keyPressed(int k);

    /**
     * Handles key release events.
     *
     * @param k the key code of the released key
     */
    public abstract void keyReleased(int k);
}
