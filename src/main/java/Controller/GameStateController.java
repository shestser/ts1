/**
 * The GameStateController class manages the different game states and controls the overall game flow.
 * It keeps track of the current game state, loads and unloads game states, and provides methods for updating, rendering,
 * and handling key events.
 */
package Controller;

import States.*;
import View.GamePanel;

import java.util.LinkedList;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class GameStateController {
    private final LinkedList<GameState> gameStates; // Linked list of game states
    private GamePanel gamePanel; // GameStateController has a GamePanel
    private int currentState; // The current game state
    private static final Logger logger = Logger.getLogger(GameStateController.class.getName());  // Logger instance

    /**
     * The total number of game states.
     */
    private static final int GAMEOVERSTATE = 3;
    public static final int MENUSTATE = 0;
    private static final int CHOOSELEVELSTATE = 1;
    private static final int LEVELRUNNINGSTATE = 2;
    private static final int STORESTATE = 4;
    private static final int PROFILESTATE = 5;
    private static final int NEWPROFILESTATE = 6;

    /**
     * Returns the state value representing the game over state.
     *
     * @return the game over state value
     */
    public static int getGameOverState() {
        return GAMEOVERSTATE;
    }

    /**
     * Returns the state value representing the menu state.
     *
     * @return the menu state value
     */
    public static int getMenuState() {
        return MENUSTATE;
    }

    /**
     * Returns the state value representing the choose level state.
     *
     * @return the choose level state value
     */
    public static int getChooseLevelState() {
        return CHOOSELEVELSTATE;
    }

    /**
     * Returns the state value representing the level running state.
     *
     * @return the level running state value
     */
    public static int getLevelRunningState() {
        return LEVELRUNNINGSTATE;
    }

    /**
     * Returns the state value representing the store state.
     *
     * @return the store state value
     */
    public static int getStoreState() {
        return STORESTATE;
    }

    /**
     * Returns the state value representing the profile state.
     *
     * @return the profile state value
     */
    public static int getProfileState() {
        return PROFILESTATE;
    }

    /**
     * Returns the state value representing the new profile state.
     *
     * @return the new profile state value
     */
    public static int getNewProfileState() {
        return NEWPROFILESTATE;
    }

    /**
     * Constructs a new GameStateController object.
     * Initializes the game states linked list, sets the current state to the profile state,
     * loads the initial game state, and sets up logging.
     */
    public GameStateController() {
        gameStates = new LinkedList<>();
        currentState = PROFILESTATE;
        loadState(currentState);

        try {
            // File handler for logging
            FileHandler fileHandler = new FileHandler("game.log");
            fileHandler.setFormatter(new SimpleFormatter());
            logger.addHandler(fileHandler);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Failed to setup logger", e);
        }
    }

    /**
     * Loads the game state based on the provided state value.
     *
     * @param state the state value representing the game state to load
     */
    private void loadState(int state) {
        if (state == PROFILESTATE)
            gameStates.add(new ProfileState(this));
        if (state == NEWPROFILESTATE)
            gameStates.add(new NewProfileState(this));
        if (state == MENUSTATE)
            gameStates.add(new MenuState(this));
        if (state == CHOOSELEVELSTATE)
            gameStates.add(new ChooseLevelState(this));
        if (state == LEVELRUNNINGSTATE)
            gameStates.add(new LevelRunningState(this));
        if (state == GAMEOVERSTATE)
            gameStates.add(new GameOverStat(this));
        if (state == STORESTATE)
            gameStates.add(new StoreState(this));
    }

    /**
     * Unloads the game state at the specified index.
     *
     * @param state the index of the game state to unload
     */
    private void unloadState(int state) {
        if (!gameStates.isEmpty()) {
            gameStates.removeLast();
        }
    }

    /**
     * Gets the GamePanel associated with the GameStateController.
     *
     * @return the GamePanel object
     */
    public GamePanel getGamePanel() {
        return gamePanel;
    }

    /**
     * Sets the state of the game to the provided state value.
     *
     * @param state the state to set
     */
    public void setState(int state) {
        unloadState(currentState);
        currentState = state;
        loadState(currentState);
    }

    /**
     * Updates the current game state.
     */
    public void update() {
        try {
            if (!gameStates.isEmpty()) {
                GameState currentState = gameStates.getLast();
                currentState.update();
            }
        } catch (Exception e) {
            // logging of the exception
            logger.log(Level.SEVERE, "An error occurred during the update process", e);
        }
    }

    /**
     * Renders the current game state.
     *
     * @param g the Graphics2D object to render with
     */
    public void render(java.awt.Graphics2D g) {
        try {
            if (!gameStates.isEmpty()) {
                GameState currentState = gameStates.getLast();
                currentState.render(g);
                gamePanel.draw();
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "An error occurred during the rendering process", e);
        }
    }

    /**
     * Handles key press events.
     *
     * @param k the key code of the pressed key
     */
    public void keyPressed(int k) {
        if (!gameStates.isEmpty()) {
            GameState currentState = gameStates.getLast();
            currentState.keyPressed(k);
        }
    }

    /**
     * Handles key release events.
     *
     * @param k the key code of the released key
     */
    public void keyReleased(int k) {
        if (!gameStates.isEmpty()) {
            GameState currentState = gameStates.getLast();
            currentState.keyReleased(k);
        }
    }

    public int getCurrentState() {
        return currentState;
    }

    /**
     * Sets the GamePanel associated with the GameStateController.
     *
     * @param gamePanel the GamePanel object to set
     */
    public void setGamePanel(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }
}