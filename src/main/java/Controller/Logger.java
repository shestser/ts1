/**
 * The Logger class provides logging functionality for the GameStateController class.
 * It handles logging of messages, exceptions, and errors.
 */
package Controller;

import java.io.IOException;
import java.util.logging.*;

public class Logger {
    private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger(GameStateController.class.getName());

    /**
     * Configures the logger by adding a file handler and setting the log level.
     */
    public static void configureLogger() {
        try {
            // Create a file handler for logging
            FileHandler fileHandler = new FileHandler("game.log");
            fileHandler.setFormatter(new SimpleFormatter());
            logger.addHandler(fileHandler);

            // Set the log level to ALL for debugging purposes
            logger.setLevel(Level.ALL);
        } catch (IOException e) {
            System.err.println("Failed to setup logger: " + e.getMessage());
        }
    }

    /**
     * Logs an information message.
     *
     * @param message the message to log
     */
    public static void info(String message) {
        logger.log(Level.INFO, message);
    }

    /**
     * Logs a warning message.
     *
     * @param message the message to log
     */
    public static void warning(String message) {
        logger.log(Level.WARNING, message);
    }

    /**
     * Logs an error message.
     *
     * @param message the message to log
     */
    public static void error(String message) {
        logger.log(Level.SEVERE, message);
    }

    /**
     * Logs an exception with a message.
     *
     * @param message   the message to log
     * @param exception the exception to log
     */
    public static void exception(String message, Exception exception) {
        logger.log(Level.SEVERE, message, exception);
    }
}