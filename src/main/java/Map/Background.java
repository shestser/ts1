package Map;

import View.GamePanel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Objects;

/**
 * The Background class represents the background image in the game.
 */
public class Background {

    private BufferedImage image;

    private double x;
    private double y;
    private double dx;
    private double dy;
    private double moveScale;

    /**
     * Constructs a Background object with the specified image and move scale.
     *
     * @param s  the path to the image file
     * @param ms the move scale for the background
     */
    public Background(String s, double ms) {

        try {
            image = ImageIO.read(
                    Objects.requireNonNull(getClass().getResourceAsStream(s))
            );
            moveScale = ms;
        }
        catch(Exception e) {
            e.printStackTrace();
        }

    }
    /**
     * Sets the position of the background image.
     *
     * @param x the x-coordinate of the position
     * @param y the y-coordinate of the position
     */
    public void setPosition(double x, double y) {
        this.x = (x * moveScale) % GamePanel.WIDTH;
        this.y = (y * moveScale) % GamePanel.HEIGHT;
    }
    /**
     * Sets the vector for the background movement.
     *
     * @param dx the x-component of the vector
     * @param dy the y-component of the vector
     */
    public void setVector(double dx, double dy) {
        this.dx = dx;
        this.dy = dy;
    }


    /**
     * Updates the position of the background based on the vector.
     */
    public void update() {
        x += dx;
        y += dy;
    }

    /**
     * Draws the background image onto the specified graphics context.
     *
     * @param g the graphics context to draw on
     */
    public void draw(Graphics2D g) {
        // Draw the background image on the given graphics context
        g.drawImage(image, (int)x, (int)y, null);

        // If the image goes beyond the left edge of the screen, draw it again on the right side
        if(x < 0) {
            g.drawImage(
                    image,
                    (int)x + GamePanel.WIDTH,
                    (int)y,
                    null
            );
        }
        // If the image goes beyond the right edge of the screen, draw it again on the left side
        if(x > 0) {
            g.drawImage(
                    image,
                    (int)x - GamePanel.WIDTH,
                    (int)y,
                    null
            );
        }
    }

}







