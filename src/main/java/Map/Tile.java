package Map;

import java.awt.image.BufferedImage;

/**
 * The Tile class represents a tile in a map.
 */

public class Tile {

    private final BufferedImage image;
    private TileType type;

    // tile types

    /**
     * Constructs a Tile object with the specified image and type.
     *
     * @param image The image associated with the tile.
     * @param type The type of the tile.
     */
    public Tile(BufferedImage image, TileType type) {
        this.image = image;
        this.type = type;
    }


    /**
     * Returns the block type of the tile.
     *
     * @return The block type of the tile.
     */
    public static int getTypeBlock() {

        return TileType.BLOCK.ordinal();
    }

    /**
     * Returns the finish type of the tile.
     *
     * @return The finish type of the tile.
     */
    public static int getTypeFinish() {
        return TileType.FINISH.ordinal();
    }

    /**
     * Returns the image associated with the tile.
     *
     * @return The image of the tile.
     */

    public BufferedImage getImage() {
        return image; }

    /**
     * Returns the type of the tile.
     *
     * @return The type of the tile.
     */
    public int getType() {
        return type.ordinal();
    }


    /**
     * Sets the type of the tile.
     *
     * @param type The new type of the tile.
     */

    public void setType(TileType type) {
        this.type = type;
    }

}
