package Map;

import View.GamePanel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;

import static Map.TileType.*;

/**

 The TileMap class represents a tile-based map.
 It provides methods to load and draw the map, as well as retrieve information about tiles and map properties.
 */
public class TileMap {

    // position
    private double x;
    private double y;

    // bounds
    private int xmin;
    private int ymin;
    private int xmax;
    private int ymax;

    private double tween;

    // map
    private int[][] map;
    private final int tileSize;
    private int numRows;
    private int numCols;

    private int numTilesAcross;
    private Tile[][] tiles;

    // drawing
    private int rowOffset;
    private int colOffset;
    private final int numRowsToDraw;
    private final int numColsToDraw;

    /**
     * Constructs a TileMap object with the specified tile size.
     *
     * @param tileSize The size of each tile in pixels.
     */
    public TileMap(int tileSize) {
        this.tileSize = tileSize;
        numRowsToDraw = GamePanel.HEIGHT / tileSize + 2;
        numColsToDraw = GamePanel.WIDTH / tileSize + 2;
        tween = 0.07;
    }

    /**
     * Loads the tileset image from the specified file path.
     *
     * @param s The file path of the tileset image.
     */
    public void loadTiles(String s) {

        try {
            // Load the tileset image from the file path
            BufferedImage tileset = ImageIO.read(
                    Objects.requireNonNull(getClass().getResourceAsStream(s)));
            // Calculate the number of tiles across the tileset image
            numTilesAcross = tileset.getWidth() / tileSize;
            // Initialize the tiles array to store the tile objects
            tiles = new Tile[2][numTilesAcross];

            BufferedImage subimage;
            // Iterate through each column in the tileset
            for (int col = 0; col < numTilesAcross; col++) {
                // Extract the subimage representing the tile from the tileset
                subimage = tileset.getSubimage(col * tileSize, 0, tileSize, tileSize);
                // Create a new Tile object with the subimage and default type
                tiles[0][col] = new Tile(subimage, HELPER);
                // Set special types for specific tiles
                if (col == 3) {
                    tiles[0][col].setType(FINISH);
                }
                if (col == 16) {
                    tiles[1][col].setType(FINISH);
                }
                // Extract the subimage representing the block tile from the tileset
                subimage = tileset.getSubimage(
                        col * tileSize,
                        tileSize,
                        tileSize,
                        tileSize
                );
                // Create a new Tile object with the block subimage and type BLOCK
                tiles[1][col] = new Tile(subimage, BLOCK);
            }

        } catch (Exception e) {
            // Print the stack trace if an exception occurs during tileset loading
            e.printStackTrace();
        }

    }

    /**
     * Loads the map data from the specified file path.
     *
     * @param s The file path of the map data.
     */
    public void loadMap(String s) {
        try {
            // Open an input stream to read the map data from the file path
            InputStream in = getClass().getResourceAsStream(s);
//            assert in != null;

            // Create a buffered reader to efficiently read the input stream
            assert in != null;
            BufferedReader br = new BufferedReader(new InputStreamReader(in));

            // Read the number of columns and rows from the map data
            numCols = Integer.parseInt(br.readLine());
            numRows = Integer.parseInt(br.readLine());

            // Create a new map array with the specified number of rows and columns
            map = new int[numRows][numCols];

            // Calculate the width and height of the map in pixels
            int width = numCols * tileSize;
            int height = numRows * tileSize;

            // Calculate the boundaries of the map within the game panel
            xmin = GamePanel.WIDTH - width;
            xmax = 0;
            ymin = GamePanel.HEIGHT - height;
            ymax = 0;

            // Define the delimiter pattern to split the map data into tokens
            String delims = "\\s+";

            // Iterate through each row in the map data
            for (int row = 0; row < numRows; row++) {
                // Read a line from the buffered reader
                String line = br.readLine();

                // Split the line into tokens using the delimiter pattern
                String[] tokens = line.split(delims);

                // Parse each token as an integer and store it in the map array
                for (int col = 0; col < numCols; col++) {
                    map[row][col] = Integer.parseInt(tokens[col]);
                }
            }

        } catch (Exception e) {
            // Print the stack trace if an exception occurs during map loading
            e.printStackTrace();
        }
    }

    /**
     * Returns the size of each tile in pixels.
     *
     * @return The tile size.
     */
    public int getTileSize() {
        return tileSize;
    }

    /**
     * Returns the current x-coordinate of the map position.
     *
     * @return The x-coordinate.
     */
    public double getx() {
        return x;
    }

    /**
     * Returns the current y-coordinate of the map position.
     *
     * @return The y-coordinate.
     */
    public double gety() {
        return y;
    }

    /**
     * Returns the type of the tile at the specified row and column.
     *
     * @param row The row index.
     * @param col The column index.
     * @return The type of the tile.
     */
    public int getType(int row, int col) {
        int rc = map[row][col];
        int r = rc / numTilesAcross;
        int c = rc % numTilesAcross;
        return tiles[r][c].getType();
    }

    /**
     * Sets the tweening factor used for smooth movement.
     *
     * @param d The tweening factor.
     */
    public void setTween(double d) {
        tween = d;
    }

    /**
     * Sets the position of the map to the specified coordinates.
     *
     * @param x The new x-coordinate.
     * @param y The new y-coordinate.
     */
    public void setPosition(double x, double y) {
        // Smoothly update the map position using the tweening factor
        this.x += (x - this.x) * tween;
        this.y += (y - this.y) * tween;

        // Ensure the map stays within the specified boundaries
        fixBounds();

        // Calculate the column and row offsets based on the map position
        colOffset = (int) -this.x / tileSize;
        rowOffset = (int) -this.y / tileSize;
    }

    /**
     * Adjusts the map position to ensure it stays within the boundaries.
     */
    private void fixBounds() {
        if (x < xmin) x = xmin;
        if (y < ymin) y = ymin;
        if (x > xmax) x = xmax;
        if (y > ymax) y = ymax;
    }

    /**
     * Draws the visible portion of the map on the specified graphics context.
     *
     * @param g The graphics context to draw on.
     */
    public void draw(Graphics2D g) {
        // Iterate through the visible rows and columns of the map
        for (int row = rowOffset; row < rowOffset + numRowsToDraw; row++) {
            if (row >= numRows) break;

            for (int col = colOffset; col < colOffset + numColsToDraw; col++) {
                if (col >= numCols) break;

                if (map[row][col] == 0) continue;

                // Get the tile at the current row and column
                int rc = map[row][col];
                int r = rc / numTilesAcross;
                int c = rc % numTilesAcross;

                // Draw the tile image at the correct position on the graphics context
                g.drawImage(
                        tiles[r][c].getImage(),
                        (int) x + col * tileSize,
                        (int) y + row * tileSize,
                        null
                );
            }
        }


    }
    public int getNumCols() {
        return numCols;
    }

    public int getNumRows() {
        return numRows;
    }
}