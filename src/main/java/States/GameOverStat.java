package States;

import Controller.GameState;
import Controller.GameStateController;
import Map.Background;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * The GameOverStat class represents the game state displayed when the game is over.
 * It extends the GameState class.
 */

public class GameOverStat extends GameState {

    private Background bg;

    private int currentChoice = 0;
    private final String[] options = {
            "Again",
            "Main menu",
            "Quit"
    };

    private Color titleColor;
    private Font titleFont;

    private Font font;

    /**
     * Constructs a GameOverStat object with the specified GameStateController.
     *
     * @param gsc The GameStateController controlling the game state.
     */
    public GameOverStat(GameStateController gsc) {

        this.gsc = gsc;

        try {

            bg = new Background("/images/Backgrounds/menubg.gif", 1);
            bg.setVector(-0.1, 0);

            titleColor = new Color(128, 0, 0);
            titleFont = new Font(
                    "Century Gothic",
                    Font.PLAIN,
                    28);

            font = new Font("Arial", Font.PLAIN, 12);

        }
        catch(Exception e) {
            e.printStackTrace();
        }

    }
    /**
     * Initializes the GameOverStat. (Empty method)
     */
    @Override
    public void init() {
        // TODO Auto-generated method stub

    }
    /**
     * Updates the GameOverStat. It updates the background.
     */
    @Override
    public void update() {
        // TODO Auto-generated method stub
        bg.update();
    }

    /**
     * Renders the GameOverStat on the specified Graphics2D object.
     *
     * @param g The Graphics2D object to render on.
     */
    @Override
    public void render(Graphics2D g) {
        // TODO Auto-generated method stub
        bg.draw(g);
        g.setColor(titleColor);
        g.setFont(titleFont);
        g.drawString("OLYMPIC GAMES", 40, 70);

        // draw menu options
        g.setFont(font);
        for(int i = 0; i < options.length; i++) {
            if(i == currentChoice) {
                g.setColor(Color.BLACK);
            }
            else {
                g.setColor(Color.RED);
            }
            g.drawString(options[i], 145, 140 + i * 15);
        }

    }

    /**
     * Selects the current choice and performs the corresponding action.
     * If the current choice is 0, it sets the game state to the LevelRunningState to play again.
     * If the current choice is 1, it sets the game state to the MenuState to go back to the main menu.
     * If the current choice is 2, it quits the game.
     */
    private void select() {
        if(currentChoice == 0) {
            gsc.setState(GameStateController.getLevelRunningState());
        }
        if(currentChoice == 1) {
            gsc.setState(GameStateController.getMenuState());
            // store
        }
        if(currentChoice == 2) {
            System.exit(0);
        }
    }
    /**
     * Handles key pressed events. It performs actions based on the pressed key.
     *
     * @param k The key code of the pressed key.
     */
    @Override
    public void keyPressed(int k) {
        if(k == KeyEvent.VK_ENTER){
            select();
        }
        if(k == KeyEvent.VK_UP) {
            currentChoice--;
            if(currentChoice == -1) {
                currentChoice = options.length - 1;
            }
        }
        if(k == KeyEvent.VK_DOWN) {
            currentChoice++;
            if(currentChoice == options.length) {
                currentChoice = 0;
            }
        }
    }
    /**
     * Handles key released events. This method is currently empty.
     *
     * @param k The key code of the released key.
     */
    @Override
    public void keyReleased(int k) {
        // TODO Auto-generated method stub

    }
}
