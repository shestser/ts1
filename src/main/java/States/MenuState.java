package States;

import Controller.GameState;
import Controller.GameStateController;
import Map.Background;

import java.awt.*;
import java.awt.event.KeyEvent;
/**

 The MenuState class represents the game state for the main menu.

 It inherits from the GameState class and provides methods for initialization, updating, rendering,

 and handling key events related to the main menu.
 */
public class MenuState extends GameState {

    private Background bg;

    int currentChoice = 0;
    final String[] options = {
            "Start",
            "Store",
            "Quit"
    };

    private Color titleColor;
    private Font titleFont;

    private Font font;

    /**
     * Constructor for the MenuState class.
     *
     * @param gsc The game state controller.
     */
    public MenuState(GameStateController gsc) {
        this.gsc = gsc;

        try {
            bg = new Background("/images/Backgrounds/menubg.gif", 1);
            bg.setVector(-0.1, 0);

            titleColor = new Color(128, 0, 0);
            titleFont = new Font("Century Gothic", Font.PLAIN, 28);

            font = new Font("Arial", Font.PLAIN, 12);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializes the menu state.
     */
    @Override
    public void init() {
        // TODO Auto-generated method stub
    }

    /**
     * Updates the menu state.
     * Updates the background.
     */
    @Override
    public void update() {
        // TODO Auto-generated method stub
        bg.update();
    }

    /**
     * Renders the menu state.
     * Draws the background, title, and menu options.
     *
     * @param g The Graphics2D object to draw on.
     */
    @Override
    public void render(Graphics2D g) {
        // TODO Auto-generated method stub
        bg.draw(g);
        g.setColor(titleColor);
        g.setFont(titleFont);
        g.drawString("OLYMPIC GAMES", 40, 70);

        // draw menu options
        g.setFont(font);
        for (int i = 0; i < options.length; i++) {
            if (i == currentChoice) {
                g.setColor(Color.RED);
            } else {
                g.setColor(Color.BLACK);
            }
            g.drawString(options[i], 145, 140 + i * 15);
        }
    }

    /**
     * Selects an option from the menu based on the current choice.
     * If the current choice is 0, sets the game state to the level selection state.
     * If the current choice is 1, sets the game state to the store state.
     * If the current choice is 2, exits the program.
     */
    void select() {
        if(currentChoice == 0) {
            gsc.setState(GameStateController.getChooseLevelState());
        }
        if(currentChoice == 1) {
            gsc.setState(GameStateController.getStoreState());
            // store
        }
        if(currentChoice == 2) {
            System.exit(0);
        }
    }
    /**
     * Handles the key pressed event.
     * Sets the player's movement direction based on the pressed key.
     *
     * @param k The keycode of the pressed key.
     */
    @Override
    public void keyPressed(int k) {
        if(k == KeyEvent.VK_ENTER){
            select();
        }
        if(k == KeyEvent.VK_UP) {
            currentChoice--;
            if(currentChoice == -1) {
                currentChoice = options.length - 1;
            }
        }
        if(k == KeyEvent.VK_DOWN) {
            currentChoice++;
            if(currentChoice == options.length) {
                currentChoice = 0;
            }
        }
    }

    /**
     * Handles the key released event.
     * Resets the player's movement direction when the key is released.
     *
     * @param k The keycode of the released key.
     */
    @Override
    public void keyReleased(int k) {
        // TODO Auto-generated method stub

    }
}
