package States;

import Controller.GameState;
import Controller.GameStateController;
import Entities.Coin;
import Entities.Obstacle;
import Entities.Player;
import Map.Background;
import Map.TileMap;
import Profile.Profile;
import Profile.ProfileManager;
import View.GamePanel;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

/**

 The LevelRunningState class represents the game state for the running level.

 It inherits from the GameState class and provides methods for initialization, updating, rendering,

 and handling key events related to the running level.
 */
public class LevelRunningState extends GameState {

    private TileMap tileMap;
    private Background bg;

    private Player player;


    private int collcoins;

    private ArrayList<Obstacle> obstacles;
    private ArrayList<Coin> coins;

    private long startTime;
    private long elapsedTime;
    private static final long LEVEL_TIME_LIMIT = 10000;


    Profile currentProfile = ProfileManager.getCurrentPlayer();

    /**
     * Constructor for the LevelRunningState class.
     *
     * @param gsc The game state controller.
     */

    public LevelRunningState(GameStateController gsc) {
        collcoins = 0;
        this.gsc = gsc;
        init();
    }

    /**
     * Initializes the game level.
     */
    public void init() {
        startTime = System.currentTimeMillis();
        tileMap = new TileMap(30);
        tileMap.loadTiles("/images/Map/run.png");
        tileMap.loadMap("/images/Map/runninglevel.map");
        tileMap.setPosition(0, 0);
        tileMap.setTween(1);
        bg = new Background("/images/Backgrounds/grassbg1.gif", 0.1);
        player = new Player(tileMap);
        player.setPosition(30, 200);
        populateObstacles();
        populateCoins();
    }


    /**
     * Populates the coins array list for the level.
     */
    private void populateCoins() {
        coins = new ArrayList<>();
        Coin o;
        Point[] points = new Point[]{
                new Point(300, 200),
                new Point(150, 200),
                new Point(65, 200),
                new Point(150, 150)
        };
        for (Point point : points) {
            o = new Coin(tileMap);
            o.setPosition(point.x, point.y);
            coins.add(o);
        }
    }

    /**
     * Populates the obstacles array list for the level.
     */
    private void populateObstacles() {
        obstacles = new ArrayList<>();
        Obstacle o;
        Point[] points = new Point[]{
                new Point(100, 200),
                new Point(240, 200),
                new Point(480, 200)

        };
        for (Point point : points) {
            o = new Obstacle(tileMap);
            o.setPosition(point.x, point.y);
            obstacles.add(o);
        }
    }

    /**
     * Updates the game state.
     * Calculates the elapsed time since the level started and checks if the time limit has been reached.
     * Updates the player's position, the tile map's position, and checks for player collisions with obstacles.
     * If the player is game over, sets the game state to "Game Over".
     * If the player has reached the finish, sets the game state to "Game Over" and updates the player's profile.
     * Updates the number of collected coins based on the player's profile goods.
     * Updates the coins collected by the player and removes collected coins from the list.
     * Updates the position of obstacles and coins.
     */
    public void update() {
        elapsedTime = System.currentTimeMillis() - startTime;
        if (elapsedTime >= LEVEL_TIME_LIMIT) {
            // Check if the time limit has been reached
            gsc.setState(GameStateController.getGameOverState());
        }

        // Update player
        player.update();
        tileMap.setPosition(GamePanel.WIDTH / 2 - player.getx(), GamePanel.HEIGHT / 2 - player.gety());

        // Set background position
        bg.setPosition(tileMap.getx(), tileMap.gety());
        player.checkHit(obstacles);

        if (player.gameOver()) {
            // Check if the player is game over
            gsc.setState(GameStateController.getGameOverState());
        }
        if (player.checkFinish()) {
            // Check if the player has reached the finish line
            gsc.setState(GameStateController.getGameOverState());
            ProfileManager profileManager = ProfileManager.getInstance();
            profileManager.loadProfiles();
            profileManager.setCurrentPlayerByName(currentProfile.getProfileName());
            if (currentProfile.getGoods().contains("SHOES")) {
                // Update collected coins based on the player's shoes
                collcoins += 5;
            }
            if (currentProfile.getGoods().contains("T-SHIRT")) {
                // Update collected coins based on the player's t-shirt
                collcoins += 10;
            }
            if (currentProfile.getGoods().contains("DOPING")) {
                // Update collected coins based on the player's doping
                collcoins *= 2;
            }
            profileManager.setCoinsForCurrentPlayer(currentProfile.getCoins() + collcoins);
        }

        player.checkHit(obstacles);
        player.checkCollect(coins);

        // Update all obstacles
        for (Obstacle e : obstacles) {
            e.update();
        }

        // Update all coins
        for (int i = 0; i < coins.size(); i++) {
            Coin c = coins.get(i);
            if (c.isCollected()) {
                // Update the number of collected coins
                collcoins++;
                coins.remove(i);
                i--;
            }
            c.update();
        }
    }

    /**
     * Handles the key pressed event.
     * Sets the player's movement direction based on the pressed key.
     *
     * @param k The keycode of the pressed key (KeyEvent.VK_XXX).
     */
    public void keyPressed(int k) {

        if (k == KeyEvent.VK_LEFT)
            player.setLeft(true);
        if (k == KeyEvent.VK_RIGHT)
            player.setRight(true);
        if (k == KeyEvent.VK_UP)
            player.setUp(true);
        if (k == KeyEvent.VK_DOWN)
            player.setDown(true);
        if (k == KeyEvent.VK_SPACE)
            player.setJumping(true);
    }

    public Player currentProfil() {
        // возвращаем экземпляр класса Player, например:
        return player;
    }

    /**
     * Handles the key released event.
     * Resets the player's movement direction when the key is released.
     *
     * @param k The keycode of the released key (KeyEvent.VK_XXX).
     */
    public void keyReleased(int k) {
        if (k == KeyEvent.VK_LEFT)
            player.setLeft(false);
        if (k == KeyEvent.VK_RIGHT)
            player.setRight(false);
        if (k == KeyEvent.VK_UP)
            player.setUp(false);
        if (k == KeyEvent.VK_DOWN)
            player.setDown(false);
        if (k == KeyEvent.VK_SPACE)
            player.setJumping(false);
    }

    /**
     * Renders the game state.
     * Displays the remaining time, draws the game panel, background, tilemap, player, obstacles, and coins.
     *
     * @param g The Graphics2D object to draw on.
     */
    @Override
    public void render(Graphics2D g) {
        long remainingTime = Math.max(0, LEVEL_TIME_LIMIT - elapsedTime);
        int seconds = (int) (remainingTime / 1000);
        String timeString = "Time: " + seconds + " seconds";
        g.setColor(Color.RED);
        g.drawString(timeString, 220, 25);
        gsc.getGamePanel().draw();

        bg.draw(g);
        // draw tilemap
        tileMap.draw(g);
        // draw player
        player.draw(g);
        // update all obstacles
        for (Obstacle obstacle : obstacles) {
            obstacle.draw(g);
        }
        // update all coins
        for (Coin coin : coins) {
            coin.draw(g);
        }
    }


    public TileMap getTileMap() {
        return tileMap;
    }

    public Background getBg() {
        return bg;
    }

    public Player getPlayer() {
        return player;
    }

    public ArrayList<Obstacle> getObstacles() {
        return obstacles;
    }

    public ArrayList<Coin> getCoins() {
        return coins;
    }
}