package States;

import Controller.GameState;
import Controller.GameStateController;
import Map.Background;
import Profile.ProfileManager;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

/**

 The NewProfileState class represents a game state for creating a new profile in the game.

 It extends the GameState class and provides methods for initialization, updating, rendering,

 and handling key events related to creating a new profile.
 */

public class NewProfileState extends GameState {

    private final ArrayList<String> options = new ArrayList<>();
    private final ProfileManager prfc = ProfileManager.getInstance();

    private Background bg;

    // instantiating the textfield objects
    // setting the location of those objects in the frame

    private int currentChoice = 0;

    private Color titleColor;
    private Font titleFont;

    private Font font;


    private String error = "";

    /**
     * Constructs a new NewProfileState object.
     *
     * @param gsc The game state controller managing this state.
     */
    public NewProfileState(GameStateController gsc) {
        this.gsc = gsc;
        init();
    }
    /**
     * Initializes the new profile state.
     * Loads the profiles.
     * Adds the options "NAME", "CREATE", and "BACK".
     * Sets up the background image, title color, title font, and regular font.
     * If an exception occurs during initialization, prints the stack trace.
     */
    @Override
    public void init() {
        prfc.loadProfiles();
        options.add("NAME");
        options.add("CREATE");
        options.add("BACK");
        try {
            bg = new Background("/images/Backgrounds/menubg.gif", 1);
            bg.setVector(-0.1, 0);

            titleColor = new Color(128, 0, 0);
            titleFont = new Font(
                    "Century Gothic",
                    Font.PLAIN,
                    28);

            font = new Font("Arial", Font.PLAIN, 12);

        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Updates the new profile state.
     * Updates the background image.
     */
    @Override
    public void update() {
        bg.update();
    }

    /**
     * Renders the new profile state.
     * Draws the background image.
     * Sets the color and font for the title and draws the title "NEW PROFILE" at coordinates (60, 70).
     * Draws the menu options and highlights the current choice.
     * Sets the color to red and draws the error message at coordinates (40, 140 + 75).
     *
     * @param g the Graphics2D object used for rendering
     */
    @Override
    public void render(Graphics2D g) {
        bg.draw(g);
        g.setColor(titleColor);
        g.setFont(titleFont);
        g.drawString("NEW PROFILE", 60, 70);

        // draw menu options
        g.setFont(font);
        g.drawString("ENTER NAME: ", 40, 140);
        for(int i = 0; i < options.size(); i++) {
            if(i == currentChoice) {
                g.setColor(Color.BLACK);
            }
            else {
                g.setColor(Color.RED);
            }
            g.drawString(options.get(i), 145, 140 + i * 15);
        }

        g.setColor(Color.RED);
        g.drawString(error, 40, 140 + 75);

    }
    /**
     * Performs an action based on the current choice.
     * If the current choice is 1 and a new profile is created successfully,
     * sets the game state to the profile state.
     * If the current choice is 1 and creating a new profile fails,
     * sets the error message to indicate the failure.
     * If the current choice is 2, sets the game state to the profile state.
     */
    private void select() {
        if(currentChoice == 1) {
            if (prfc.createNewProfile(options.get(0))) {
                gsc.setState(GameStateController.getProfileState());
            } else {
                error = "NAME EXISTS OR PLAYERS LIMIT IS REACHED!";
            }
        }
        if(currentChoice == 2) {
            gsc.setState(GameStateController.getProfileState());
        }
    }

    /**
     * Handles the key pressed event.
     * Sets the player's movement direction based on the pressed key.
     *
     * @param k The keycode of the pressed key (KeyEvent.VK_XXX).
     */
    @Override
    public void keyPressed(int k) {
        if(k == KeyEvent.VK_ENTER){
            select();
        }

        if(k >= KeyEvent.VK_A && k <= KeyEvent.VK_Z && currentChoice == 0 ){
            if (options.get(0).length() < 10) {
                options.set(0, options.get(0) + KeyEvent.getKeyText(k));
            }
        }

        if(k == KeyEvent.VK_BACK_SPACE && currentChoice == 0 ){
            if (options.get(0).length() != 0) {
                options.set(0, options.get(0).substring(0, options.get(0).length()-1));
            }
        }

        if(k == KeyEvent.VK_UP) {
            currentChoice--;
            if(currentChoice == -1) {
                currentChoice = options.size() - 1;
            }
        }
        if(k == KeyEvent.VK_DOWN) {
            currentChoice++;
            if(currentChoice == options.size()) {
                currentChoice = 0;
            }
        }
    }

    /**
     * Handles the key released event.
     * Resets the player's movement direction when the key is released.
     *
     * @param k The keycode of the released key
     */
    @Override
    public void keyReleased(int k) {
        // TODO Auto-generated method stub

    }

}
