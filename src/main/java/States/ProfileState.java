package States;

import Controller.GameState;
import Controller.GameStateController;
import Map.Background;
import Profile.Profile;
import Profile.ProfileManager;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;


/**

 The ProfileState class represents a game state where players can choose or create profiles.

 It extends the GameState class and provides methods for initialization, updating, rendering,

 and handling key events related to the profile selection menu.
 */
public class ProfileState extends GameState {

    private final ArrayList<String> options = new ArrayList<>();

    private Background bg;

    private int currentChoice = 0;

    private Color titleColor;
    private Font titleFont;

    private Font font;

    /**

     Constructs a ProfileState object with the specified GameStateController.
     Initializes the menu options and sets the initial current choice.
     @param gsc The GameStateController object for managing game states.
     */

    public ProfileState(GameStateController gsc) {
        this.gsc = gsc;
        options.add("NEW +");
        init();
        ProfileManager profileManager = ProfileManager.getInstance();
    }

    /**

     Initializes the profile state by loading existing profiles and setting up the background,

     title color, fonts, and menu options.
     */
    @Override
    public void init() {
        ProfileManager profileManager = ProfileManager.getInstance();
        profileManager.loadProfiles();
        for (Profile p : profileManager.getProfiles() ) {
            options.add(p.getProfileName());
        }
        options.add("QUIT");

        try {

            bg = new Background("/images/Backgrounds/menubg.gif", 1);
            bg.setVector(-0.1, 0);

            titleColor = new Color(128, 0, 0);
            titleFont = new Font(
                    "Century Gothic",
                    Font.PLAIN,
                    28);

            font = new Font("Arial", Font.PLAIN, 12);

        }
        catch(Exception e) {
            e.printStackTrace();
        }
        // TODO Auto-generated method stub

    }
    /**

     Updates the background animation of the profile state.
     */
    @Override
    public void update() {
        bg.update();
    }

    /**

     Renders the profile state by drawing the background, title, and menu options on the graphics context.

     @param g The Graphics2D object to draw on.
     */
    @Override
    public void render(Graphics2D g) {
        bg.draw(g);
        g.setColor(titleColor);
        g.setFont(titleFont);
        g.drawString("CHOOSE PROFILE", 40, 70);

        // draw menu options
        g.setFont(font);
        for(int i = 0; i < options.size(); i++) {
            if(i == currentChoice) {
                g.setColor(Color.BLACK);
            }
            else {
                g.setColor(Color.RED);
            }
            g.drawString(options.get(i), 145, 140 + i * 15);
        }
    }
    /**

     Handles the selection of a menu option based on the current choice.
     If "NEW +" is chosen, it transitions to the new profile state.
     If a profile option is chosen, it sets the current player and transitions to the menu state.
     If "QUIT" is chosen, it exits the game.
     */
    private void select() {
        if(currentChoice == 0) {
            gsc.setState(GameStateController.getNewProfileState());
        }
        if(currentChoice > 0 && currentChoice < options.size() - 1) {
            ProfileManager.getInstance().setCurrentPlayerByName(options.get(currentChoice));
            gsc.setState(GameStateController.getMenuState());
        }
        if(currentChoice == options.size() - 1) {
            System.exit(0);
        }
    }
    /**
     * Handles the key pressed event.
     * Sets the player's movement direction based on the pressed key.
     *
     * @param k The keycode of the pressed key.
     */
    @Override
    public void keyPressed(int k) {
        if(k == KeyEvent.VK_ENTER){
            select();
        }
        if(k == KeyEvent.VK_UP) {
            currentChoice--;
            if(currentChoice == -1) {
                currentChoice = options.size() - 1;
            }
        }
        if(k == KeyEvent.VK_DOWN) {
            currentChoice++;
            if(currentChoice == options.size()) {
                currentChoice = 0;
            }
        }
        if(k == KeyEvent.VK_DELETE && currentChoice != 0 && currentChoice != options.size()-1) {
            ProfileManager.getInstance().removeProfileByName(options.get(currentChoice));
            options.remove(currentChoice);
        }
    }
    /**
     * Handles the key released event.
     * Resets the player's movement direction when the key is released.
     *
     * @param k The keycode of the released key
     */
    @Override
    public void keyReleased(int k) {
        // TODO Auto-generated method stub

    }

}