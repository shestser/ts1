package States;

import Controller.GameState;
import Controller.GameStateController;
import Map.Background;
import Profile.Profile;
import Profile.ProfileManager;

import java.awt.*;
import java.awt.event.KeyEvent;

/**

 The StoreState class represents a game state where players can buy items from the store.

 It extends the GameState class and provides methods for initialization, updating, rendering,

 and handling key events related to the store menu.
 */

public class StoreState extends GameState {

    public String errorMessage;
    private Background bg;
    private final ProfileManager profileManager;

    Profile currentProfile = ProfileManager.getCurrentPlayer();

    int currentChoice = 0;
    private final String[] options = {
            "SHOES 150 coins(+5)",
            "T-SHORT 200 coins(+10)",
            "DOPING 300 coins(х2)",
            "Back"
    };

    private Color titleColor;
    private Font titleFont;

    private Font font;


    public StoreState(GameStateController gsc) {
        this.gsc = gsc;
        profileManager = ProfileManager.getInstance();

        try {
            bg = new Background("/images/Backgrounds/menubg.gif", 1);
            bg.setVector(-0.1, 0);

            titleColor = new Color(128, 0, 0);
            titleFont = new Font("Century Gothic", Font.PLAIN, 28);

            font = new Font("Arial", Font.PLAIN, 12);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() {
        profileManager.loadProfiles();

    }

    @Override
    public void update() {
        bg.update();
    }

    @Override
    public void render(Graphics2D g) {
        bg.draw(g);
        g.setColor(titleColor);
        g.setFont(font);

        g.drawString("YOUR ITEMS:", 20, 95);
        int y = 115;
        for (String item : currentProfile.getGoods()) {
            g.drawString(item, 20, y);
            y += 35;
        }
        g.setFont(titleFont);
        Profile currentProfile = ProfileManager.getCurrentPlayer();
        g.drawString("WELCOME " + currentProfile.getProfileName(), 1, 30);
        g.drawString("YOUR BALANCE: "+  currentProfile.getCoins(),1,55);

        g.setFont(font);

        for (int i = 0; i < options.length; i++) {
            if (i == currentChoice) {
                g.setColor(Color.BLACK);
            } else {
                g.setColor(Color.RED);
            }
            g.drawString(options[i], 145, 140 + i * 15);
        }
    }


    void select() {
        gsc =new GameStateController();
        String[] itemNames = {"SHOES", "T-SHIRT", "DOPING"};
        int[] itemPrices = {150, 200, 300};
        String errorMessage = "";
        for (int i = 0; i < itemNames.length; i++) {
            if (currentChoice == i) {
                if (currentProfile.getGoods().contains(itemNames[i])) {
                    errorMessage = "You already have " + itemNames[i];
                } else if (currentProfile.getCoins() < itemPrices[i]) {
                    errorMessage = "Not enough coins to buy " + itemNames[i];
                } else {
                    ProfileManager profileManager = ProfileManager.getInstance();
                    profileManager.loadProfiles();
                    profileManager.setCurrentPlayerByName(currentProfile.getProfileName());
                    profileManager.addGoodToCurrentPlayer(itemNames[i]);
                    profileManager.setCoinsForCurrentPlayer(currentProfile.getCoins() - itemPrices[i]);


//                    gsc.setState(GameStateController.getStoreState());
                    gsc.setState(0);

                    currentProfile.setCoins(currentProfile.getCoins() - itemPrices[i]);
                }
            }
            if (currentChoice == 3) {gsc.setState(GameStateController.getMenuState());}
        }

        if (!errorMessage.equals("")) {
            System.out.println(errorMessage);
        }
    }

    @Override
    public void keyPressed(int k) {
        if (k == KeyEvent.VK_ENTER) {
            select();
        }
        if (k == KeyEvent.VK_UP) {
            currentChoice--;
            if (currentChoice == -1) {
                currentChoice = options.length - 1;
            }
        }
        if (k == KeyEvent.VK_DOWN) {
            currentChoice++;
            if (currentChoice == options.length) {
                currentChoice = 0;
            }
        }
    }

    @Override
    public void keyReleased(int k) {

    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
