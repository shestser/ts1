package Entities;

import Map.TileMap;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Objects;

/**
 * The Obstacle class represents a static object in the game map that the player cannot pass through.
 * It extends the MapObject class.
 */
public class Obstacle extends MapObject {

    private BufferedImage[] sprites;

    /**
     * Constructs an Obstacle object with the specified TileMap.
     *
     * @param tm The TileMap representing the game map.
     */
    public Obstacle(TileMap tm) {
        super(tm);
        width = 30;
        height = 30;
        cwidth = 15;
        cheight = 15;


        try {
            sprites = new BufferedImage[1];
            BufferedImage spritesheet = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/images/Obstacles/barier.png")));
            sprites[0] = spritesheet.getSubimage(0, 0, width, height);
        } catch (Exception e) {
            e.printStackTrace();
        }

        animation = new Animation();
        animation.setFrames(sprites);
        animation.setDelay(300);
    }

    /**
     * Updates the state of the Obstacle.
     */
    public void update() {
        checkTileMapCollision();
        setPosition(xtemp, ytemp);
    }

    /**
     * Draws the Obstacle onto the specified Graphics2D object.
     *
     * @param g The Graphics2D object to draw on.
     */
    public void draw(Graphics2D g) {
        setMapPosition();
        super.draw(g);
    }

}