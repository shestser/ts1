package Entities;

import Map.Tile;
import Map.TileMap;

import java.awt.*;

/**
 * An abstract class representing a map object.
 * This class serves as a base for all objects on the game map.
 */
public abstract class MapObject {

    // tile stuff
    protected TileMap tileMap;
    protected int tileSize;
    protected double xmap;
    protected double ymap;

    // position and vector
    protected double x;
    protected double y;
    protected double dx;
    protected double dy;

    // dimensions
    protected int width;
    protected int height;

    // collision box
    protected int cwidth;
    protected int cheight;

    // collision
    protected int currRow;
    protected int currCol;
    protected double xdest;
    protected double ydest;
    protected double xtemp;
    protected double ytemp;
    protected boolean topLeft;
    protected boolean topRight;
    protected boolean bottomLeft;
    protected boolean bottomRight;

    // animation
    protected Animation animation;
    protected int currentAction;
    protected boolean facingRight;

    // movement
    protected boolean left;
    protected boolean right;
    protected boolean up;
    protected boolean down;
    protected boolean jumping;
    protected boolean falling;

    // movement attributes
    protected double moveSpeed;
    protected double maxSpeed;
    protected double stopSpeed;
    protected double fallSpeed;
    protected double maxFallSpeed;
    protected double jumpStart;
    protected double stopJumpSpeed;

    /**
     * Constructor for the MapObject class.
     *
     * @param tm The TileMap on which the object resides.
     */
    MapObject(TileMap tm) {
        tileMap = tm;
        tileSize = tm.getTileSize();
    }

    /**
     * Checks if this object intersects with another object on the map.
     *
     * @param o The other object to check for intersection.
     * @return true if the objects intersect, false otherwise.
     */
    public boolean intersects(MapObject o) {
        Rectangle r1 = getRectangle();
        Rectangle r2 = o.getRectangle();
        return r1.intersects(r2);
    }

    /**
     * Returns the rectangle representing the boundaries of the object.
     *
     * @return The rectangle encompassing the object.
     */
    Rectangle getRectangle() {
        return new Rectangle(
                (int)x - cwidth,
                (int)y - cheight,
                cwidth,
                cheight
        );
    }

    /**
     * Calculates the tile corners around the specified coordinates.
     *
     * @param x The x-coordinate.
     * @param y The y-coordinate.
     */
    void calculateCorners(double x, double y) {
        int leftTile = (int)(x - cwidth / 2) / tileSize;
        int rightTile = (int)(x + cwidth / 2 - 1) / tileSize;
        int bottomTile = (int)(y + cheight / 2 - 1) / tileSize;

        int bl = tileMap.getType(bottomTile, leftTile);
        int br = tileMap.getType(bottomTile, rightTile);

        bottomLeft = bl == Tile.getTypeBlock();
        bottomRight = br == Tile.getTypeBlock();
    }

    /**
     * Calculates the collision detection and response with the tile map.
     * Adjusts the object's position based on the collision results.
     */
    void checkTileMapCollision() {
        // Determine the current column and row of the object in the tile map
        currCol = (int) x / tileSize;
        currRow = (int) y / tileSize;

        // Calculate the destination position of the object
        xdest = x + dx;
        ydest = y + dy;

        // Store the current position of the object
        xtemp = x;
        ytemp = y;

        // Check for collision with tiles in the vertical direction (y-axis)
        calculateCorners(x, ydest);
        if (dy < 0) {
            // Object is moving upwards
            if (topLeft || topRight) {
                // Collision with a tile above, stop the upward movement
                dy = 0;
                ytemp = currRow * tileSize + cheight / 2;
            } else {
                // No collision, continue moving upwards
                ytemp += dy;
            }
        }
        if (dy > 0) {
            // Object is moving downwards
            if (bottomLeft || bottomRight) {
                // Collision with a tile below, stop the downward movement
                dy = 0;
                falling = false;
                ytemp = (currRow + 1) * tileSize - cheight / 2;
            } else {
                // No collision, continue moving downwards
                ytemp += dy;
            }
        }

        // Check for collision with tiles in the horizontal direction (x-axis)
        calculateCorners(xdest, y);
        if (dx < 0) {
            // Object is moving to the left
            if (topLeft || bottomLeft) {
                // Collision with a tile to the left, stop the leftward movement
                dx = 0;
                xtemp = currCol * tileSize + cwidth / 2;
            } else {
                // No collision, continue moving to the left
                xtemp += dx;
            }
        }
        if (dx > 0) {
            // Object is moving to the right
            if (topRight || bottomRight) {
                // Collision with a tile to the right, stop the rightward movement
                dx = 0;
                xtemp = (currCol + 1) * tileSize - cwidth / 2;
            } else {
                // No collision, continue moving to the right
                xtemp += dx;
            }
        }

        // Check if the object is falling (not standing on any tiles)
        if (!falling) {
            calculateCorners(x, ydest + 1);
            if (!bottomLeft && !bottomRight) {
                // No tile below, the object is falling
                falling = true;
            }
        }
    }

    /**
     * Returns the x-coordinate of the object's position.
     *
     * @return The x-coordinate of the object's position.
     */
    public int getx() {
        return (int) x;
    }

    /**
     * Returns the y-coordinate of the object's position.
     *
     * @return The y-coordinate of the object's position.
     */
    public int gety() {
        return (int) y;
    }

    /**
     * Sets the position of the object to the specified coordinates.
     *
     * @param x The x-coordinate of the new position.
     * @param y The y-coordinate of the new position.
     */
    public void setPosition(double x, double y) {
        this.x = x;
        this.y = y;
    }
    /**
     * Sets the map position of the object based on the current position of the tile map.
     * This method is responsible for updating the xmap and ymap variables, which represent
     * the position of the tile map on the screen.
     */
    void setMapPosition() {
        xmap = tileMap.getx();
        ymap = tileMap.gety();
    }

    /**
     * Sets the left movement of the object.
     *
     * @param b true to indicate left movement, false otherwise.
     */
    public void setLeft(boolean b) {
        left = b;
    }

    /**
     * Sets the right movement of the object.
     *
     * @param b true to indicate right movement, false otherwise.
     */
    public void setRight(boolean b) {
        right = b;
    }

    /**
     * Sets the upward movement of the object.
     *
     * @param b true to indicate upward movement, false otherwise.
     */
    public void setUp(boolean b) {
        up = b;
    }

    /**
     * Sets the downward movement of the object.
     *
     * @param b true to indicate downward movement, false otherwise.
     */
    public void setDown(boolean b) {
        down = b;
    }

    /**
     * Sets the jumping state of the object.
     *
     * @param b true to indicate jumping, false otherwise.
     */
    public void setJumping(boolean b) {
        jumping = b;
    }

    /**
     * Draws the object on the graphics context.
     *
     * @param g The graphics context on which to draw the object.
     */
    void draw(java.awt.Graphics2D g) {
        if (facingRight) {
            g.drawImage(
                    animation.getImage(),
                    (int) (x + xmap - width / 2),
                    (int) (y + ymap - height / 2),
                    null
            );
        } else {
            g.drawImage(
                    animation.getImage(),
                    (int) (x + xmap - width / 2 + width),
                    (int) (y + ymap - height / 2),
                    -width,
                    height,
                    null
            );
        }
    }
}