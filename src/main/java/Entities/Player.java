package Entities;

import Map.Tile;
import Map.TileMap;
import Profile.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Objects;

/**
 * The Player class represents the main player character in the game.
 * It extends the MapObject class.
 */
public class Player extends MapObject {

    private boolean has;

    private int health;
    private boolean flinching;
    private long flinchTimer;
    private ArrayList<BufferedImage[]> sprites;
    private static final int RUNNING = 0;
    private static final int JUMPING = 1;
    private static final int STANDING = 2;

    /**
     * Constructs a Player object with the specified TileMap.
     *
     * @param tm The TileMap representing the game map.
     */
    public Player(TileMap tm) {
        super(tm);
        width = 30;
        height = 30;
        cwidth = 20;
        cheight = 20;

        moveSpeed = 0.3;
        maxSpeed = 1.6;
        stopSpeed = 0.4;
        fallSpeed = 0.15;
        maxFallSpeed = 4.0;
        jumpStart = -3.3;
        stopJumpSpeed = 0.3;

        facingRight = true;

        health = 3;

        try {
            BufferedImage spritesheet = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/images/Player/beguny.png")));
            sprites = new ArrayList<>();

            for (int i = 0; i < 3; i++) {
                int[] numFrames = {2, 1, 1, 1};
                BufferedImage[] bi = new BufferedImage[numFrames[i]];

                for (int j = 0; j < numFrames[i]; j++) {
                    bi[j] = spritesheet.getSubimage(j * width, i * height, width, height);
                }

                sprites.add(bi);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        animation = new Animation();
        currentAction = STANDING;
        animation.setFrames(sprites.get(STANDING));
        animation.setDelay(400);
    }

    /**
     * Checks if the player collides with any obstacles and updates the player's health accordingly.
     *
     * @param obstacles The list of obstacles in the game.
     */
    /**
     * Checks if the player collides with any obstacles and updates the player's health accordingly.
     *
     * @param obstacles The list of obstacles in the game.
     */
    public void checkHit(ArrayList<Obstacle> obstacles) {
        boolean hitThisFrame = false;

        for (Obstacle o : obstacles) {
            // Check if the player intersects with an obstacle
            if (intersects(o) && !hitThisFrame) {
                if (!flinching) {
                    hitThisFrame = true;
                    // Reduce the player's health and log the remaining health
                    health--;
                    System.out.println("-1 life, you have " + health);
                    // Set the player to a flinching state and start the flinch timer
                    flinching = true;
                    flinchTimer = System.nanoTime();
                }
            }
        }
    }

    /**
     * Checks if the player collects any coins and logs the event.
     *
     * @param coins The list of coins in the game.
     */
    public void checkCollect(ArrayList<Coin> coins) {
        for (Coin c : coins) {
            // Check if the player intersects with a coin
            if (intersects(c)) {
                // Collect the coin and log the pickup event
                c.collect();
                Logger.getInstance().logCoinPickup(x, y);
            }
        }
    }

    /**
     * Checks if the player has reached the finish line.
     *
     * @return true if the player has reached the finish line, false otherwise.
     */
    public boolean checkFinish() {
        int row = (int) (y / tileSize);
        int col = (int) (x / tileSize);
        int currTile = tileMap.getType(row, col);

        // Check if the current tile is the finish tile
        if (currTile == Tile.getTypeFinish()) {
            System.out.println("You crossed the finish line!");
            return true;
        }
        return false;
    }

    public boolean gameOver() {
        return health == 0;
    }

    private void getNextPosition() {
        // movement
        if (left) {
            dx -= moveSpeed;
            if (dx < -maxSpeed) {
                dx = -maxSpeed;
            }
        } else if (right) {
            dx += moveSpeed;
            if (dx > maxSpeed) {
                dx = maxSpeed;
            }
        } else {
            if (dx > 0) {
                dx -= stopSpeed;
                if (dx < 0) {
                    dx = 0;
                }
            } else if (dx < 0) {
                dx += stopSpeed;
                if (dx > 0) {
                    dx = 0;
                }
            }
        }

        // jumping
        if (jumping && !falling) {
            dy = jumpStart;
            falling = true;
        }

        // falling
        if (falling) {
            dy += fallSpeed;

            if (dy > 0)
                jumping = false;
            if (dy < 0 && !jumping)
                dy += stopJumpSpeed;

            if (dy > maxFallSpeed)
                dy = maxFallSpeed;
        }


    }

    /**
     * Updates the state of the player.
     * This method is called repeatedly to update the player's position, animation, and other properties.
     */
    public void update() {
        // Update position
        Logger.getInstance().logPlayerPosition(x, y);
        getNextPosition();
        checkTileMapCollision();
        setPosition(xtemp, ytemp);

        // Handle flinching
        if (flinching) {
            long elapsed = (System.nanoTime() - flinchTimer) / 1000000;
            if (elapsed > 1000) {
                flinching = false;
            }
        }

        // Update animation based on player's movement
        if (dy < 0) {
            if (currentAction != JUMPING) {
                currentAction = JUMPING;
                animation.setFrames(sprites.get(JUMPING));
                animation.setDelay(-1);
                width = 30;
            }
        } else if (!jumping && (left || right)) {
            if (currentAction != RUNNING) {
                currentAction = RUNNING;
                animation.setFrames(sprites.get(RUNNING));
                animation.setDelay(300);
                width = 30;
            }
        } else {
            if (currentAction != STANDING) {
                currentAction = STANDING;
                animation.setFrames(sprites.get(STANDING));
                animation.setDelay(-1);
                width = 30;
            }
        }

        // Update animation frame
        animation.update();

        // Set player's facing direction
        if (right)
            facingRight = true;
        if (left)
            facingRight = false;
    }

    /**
     * Draws the player onto the graphics context.
     * This method is called to render the player on the screen.
     *
     * @param g The Graphics2D object to draw on.
     */
    public void draw(Graphics2D g) {
        setMapPosition();

        // Handle flinching animation
        if (flinching) {
            long elapsed = (System.nanoTime() - flinchTimer) / 1000000;
            if (elapsed / 100 % 2 == 0) {
                return;
            }
        }

        // Draw player
        super.draw(g);
    }
}