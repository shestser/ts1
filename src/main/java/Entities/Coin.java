package Entities;

import Map.TileMap;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Objects;

/**
 * The Coin class represents a collectible coin in the game.
 * It extends the MapObject class and inherits its properties and methods.
 */

public class Coin extends MapObject {

    private BufferedImage[] sprites;  // The array of coin sprites
    boolean collected; // Flag indicating if the coin has been collected
    int value; // The value of the coin

    /**
     * Constructs a Coin object with the specified TileMap.
     *
     * @param tm The TileMap object that the coin belongs to.
     */

    public Coin(TileMap tm) {
        super(tm);
        width = 30;
        height = 30;
        cwidth = 20;
        cheight = 20;
        value = 1;

        try {
            sprites = new BufferedImage[4];
            BufferedImage spritesheet = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/images/Map/moneta.png")));
            for(int i = 0; i < sprites.length; i++) {
                sprites[i] = spritesheet.getSubimage(
                        i * width,
                        0,
                        width,
                        height
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        animation = new Animation();
        animation.setFrames(sprites);
        animation.setDelay(300);
    }

    /**
     * Checks if the coin has been collected.
     *
     * @return true if the coin has been collected, false otherwise.
     */

    public boolean isCollected() { return collected; }

    /**
     * Sets the collected flag to true, indicating that the coin has been collected.
     */

    public void collect() {
        collected = true;
    }

    /**
     * Updates the position of the coin and its animation.
     */
    public void update() {
        checkTileMapCollision();
        setPosition(xtemp, ytemp);
        animation.update();
    }
    /**
     * Draws the coin on the screen using the provided Graphics2D object.
     *
     * @param g The Graphics2D object to draw the coin.
     */
    public void draw(Graphics2D g) {
        setMapPosition();
        super.draw(g);

    }
}