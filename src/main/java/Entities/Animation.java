package Entities;

import java.awt.image.BufferedImage;

/**
 * The Animation class represents a sequence of frames that can be played in order.
 * Each frame is a BufferedImage object.
 */
public class Animation {

    private BufferedImage[] frames; // The array of frames
    private int currentFrame; // The index of the current frame being displayed

    private long startTime; // The time at which the animation started
    private long delay; // The delay between frames in milliseconds


    /**
     * Sets the frames for the animation.
     *
     * @param frames An array of BufferedImages representing the frames of the animation.
     */
    public void setFrames(BufferedImage[] frames) {
        this.frames = frames;
        currentFrame = 0;
        startTime = System.nanoTime();
    }

    /**
     * Sets the delay between frames.
     *
     * @param d The delay between frames in milliseconds.
     */

    public void setDelay(long d) { delay = d; }

    /**
     * Updates the animation by advancing to the next frame if the delay has passed.
     * If the animation reaches the last frame, it starts over from the beginning.
     */
    public void update() {

        if(delay == -1) return;

        long elapsed = (System.nanoTime() - startTime) / 1000000;
        if(elapsed > delay) {
            currentFrame++;
            startTime = System.nanoTime();
        }
        if(currentFrame == frames.length) {
            currentFrame = 0;
        }

    }
    /**
     * Returns the BufferedImage representing the current frame.
     *
     * @return The BufferedImage representing the current frame.
     */
    public BufferedImage getImage() { return frames[currentFrame]; }
}
