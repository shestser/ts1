package Profile;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

/**This class is responsible for the logic of working with profiles,
 *  such as creating, deleting and adding an item to a profile.    */
public class ProfileManager {

    private final ArrayList<Profile> profiles;
    private static Profile currentPlayer;
    public static ProfileManager instance;
    private String filePath = "/json/players.json";
    private String tempFilePath = "temp.json";

    /**
     * Constructs a new ProfileManager object.
     * Initializes the list of profiles.
     */
    private ProfileManager() {
        profiles = new ArrayList<>();
    }

    public static synchronized ProfileManager getInstance() {
        if(instance == null){
            instance = new ProfileManager();
        }
        return instance;
    }

    /**
     * Loads player profiles from a JSON file and populates the profiles list.
     * @throws org.json.simple.parser.ParseException 
     */
    public void loadProfiles() {
        profiles.clear(); // Clears the profile list before loading

        URL resourceUrl = getClass().getResource(filePath);
        if (resourceUrl == null) {
            File newFile = new File(filePath);
            try {
                if (newFile.createNewFile()) {
                    System.out.println("New file created: " + newFile.getAbsolutePath());
                } else {
                    System.out.println("Failed to create the new file.");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (InputStream inputStream = getClass().getResourceAsStream(filePath);
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(inputStreamReader)) {
               // Read JSON file
               StringBuilder stringBuilder = new StringBuilder();


               String line;
               while ((line = reader.readLine()) != null) {
                   stringBuilder.append(line);
               }
               reader.close();

               String jsonString = stringBuilder.toString();

               JSONParser jsonParser = new JSONParser();
               JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonString);

               System.out.println(jsonObject);

               // Process the JSON object as needed
               jsonObject.forEach(plr -> parseProfileObject((JSONObject) plr));

           } catch (IOException | ParseException e) {
               e.printStackTrace();
           }
    }

    /**
     * Sets the current player based on the given name.
     *
     * @param name The name of the player profile to set as the current player.
     */
    public void setCurrentPlayerByName(String name) {
        for (Profile p : profiles) {
            if (p.getProfileName().equalsIgnoreCase(name)) {
                setCurrentPlayer(p);
            }
        }
    }

    /**
     * Removes a player profile from the profiles list based on the given name.
     * Updates the JSON file after removing the profile.
     *
     * @param name The name of the player profile to remove.
     */
    public void removeProfileByName(String name) {
        int removeAtIndex = 0;
        for (; removeAtIndex < profiles.size() - 1; removeAtIndex++) {
            if (profiles.get(removeAtIndex).getProfileName().equalsIgnoreCase(name))
                break;
        }
        profiles.remove(removeAtIndex);

        JSONArray jsonArray = new JSONArray();

        // Rebuild the JSON array after removing the profile
        for (Profile p : profiles) {
            JSONObject playerDetails = new JSONObject();
            playerDetails.put("name", p.getProfileName());
            playerDetails.put("coins", p.getCoins());
            playerDetails.put("goods", p.getGoods());

            JSONObject playerObject = new JSONObject();
            playerObject.put("player", playerDetails);

            jsonArray.add(playerObject);
        }
        
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(tempFilePath))) {
            writer.write(jsonArray.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            File originalFile = new File(getClass().getResource(filePath).toURI());
            File tempFile = new File(tempFilePath);
            if (originalFile.exists()) {
                Files.copy(tempFile.toPath(), originalFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                System.out.println("File updated successfully.");
            } else {
                System.out.println("Original file not found.");
            }
            tempFile.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new player profile with the given name.
     * Adds the profile to the profiles list and updates the JSON file.
     *
     * @param name The name of the new player profile to create.
     * @return True if the profile was created successfully, false otherwise.
     */
    public boolean createNewProfile(String name) {
        if (profiles.size() == 5) {
            System.out.println("No more space!");
            return false;
        }
        for (Profile p : profiles) {
            if (p.getProfileName().equalsIgnoreCase(name)) {
                System.out.println("Such name exists!");
                return false;
            }
        }
        
        Profile p = new Profile(name);
        profiles.add(p);
        JSONObject playerDetails = new JSONObject();
        playerDetails.put("name", p.getProfileName());
        playerDetails.put("coins", p.getCoins());
        playerDetails.put("goods", p.getGoods());
        
        JSONObject playerObject = new JSONObject();
        playerObject.put("player", playerDetails);

        JSONParser jsonParser = new JSONParser();
        
        try (InputStream inputStream = getClass().getResourceAsStream(filePath);
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(inputStreamReader);
                BufferedWriter writer = new BufferedWriter(new FileWriter(tempFilePath))) {

            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            String jsonString = stringBuilder.toString();
            JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonString);
            jsonObject.add(playerObject);
            writer.write(jsonObject.toJSONString());
            
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        try {
            File originalFile = new File(getClass().getResource(filePath).toURI());
            File tempFile = new File(tempFilePath);
            if (originalFile.exists()) {
                Files.copy(tempFile.toPath(), originalFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                System.out.println("File updated successfully.");
            } else {
                System.out.println("Original file not found.");
            }
            tempFile.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
    /**
     * Parses a profile object from JSON and adds it to the profile list.
     *
     * @param plr JSON profile object
     */
    private void parseProfileObject(JSONObject plr) {
    	JSONObject playerObject = (JSONObject) plr.get("player");
        String name = (String) playerObject.get("name");
        Long coins = (Long) playerObject.get("coins");
        ArrayList<String> goods = (ArrayList<String>) playerObject.get("goods");
        Profile prf = new Profile(name, coins, goods);
        profiles.add(prf);
        
        for (Profile p : profiles) {
        	System.out.println(p.getProfileName() + " - " + p.getCoins() + " - " + p.getGoods());
        }
        System.out.println(profiles.size());
    }
    /**
     * Adds a good to the current player's goods list.
     *
     * @param goodName the name of the good to add
     */
    public void addGoodToCurrentPlayer(String goodName) {
        if (currentPlayer.getGoods().contains(goodName)) {
            System.out.println("You already have " + goodName);
            return;
        }
        currentPlayer.getGoods().add(goodName);

        JSONObject playerDetails = new JSONObject();
        playerDetails.put("name", currentPlayer.getProfileName());
        playerDetails.put("coins", currentPlayer.getCoins());
        playerDetails.put("goods", currentPlayer.getGoods());

        JSONObject playerObject = new JSONObject();
        playerObject.put("player", playerDetails);
        // Create a JSON array to hold all player objects
        JSONArray jsonArray = new JSONArray();
        for (Profile p : profiles) {
            if (p == currentPlayer) {
                jsonArray.add(playerObject);
            } else {
                // Create a JSON object for other players' details
                JSONObject otherPlayerDetails = new JSONObject();
                otherPlayerDetails.put("name", p.getProfileName());
                otherPlayerDetails.put("coins", p.getCoins());
                otherPlayerDetails.put("goods", p.getGoods());
                // Create a JSON object for other players
                JSONObject otherPlayerObject = new JSONObject();
                otherPlayerObject.put("player", otherPlayerDetails);

                jsonArray.add(otherPlayerObject);
            }
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(tempFilePath))) {
            writer.write(jsonArray.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            File originalFile = new File(getClass().getResource(filePath).toURI());
            File tempFile = new File(tempFilePath);
            if (originalFile.exists()) {
                Files.copy(tempFile.toPath(), originalFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                System.out.println("File updated successfully.");
            } else {
                System.out.println("Original file not found.");
            }
            tempFile.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Sets the number of coins for the current player.
     *
     * @param coins the number of coins to set
     */
    public void setCoinsForCurrentPlayer(long coins) {
        currentPlayer.setCoins(coins);

        JSONObject playerDetails = new JSONObject();
        playerDetails.put("name", currentPlayer.getProfileName());
        playerDetails.put("coins", currentPlayer.getCoins());
        playerDetails.put("goods", currentPlayer.getGoods());

        JSONObject playerObject = new JSONObject();
        playerObject.put("player", playerDetails);

        JSONArray jsonArray = new JSONArray();
        for (Profile p : profiles) {
            if (p == currentPlayer) {
                jsonArray.add(playerObject);
            } else {
                JSONObject otherPlayerDetails = new JSONObject();
                otherPlayerDetails.put("name", p.getProfileName());
                otherPlayerDetails.put("coins", p.getCoins());
                otherPlayerDetails.put("goods", p.getGoods());

                JSONObject otherPlayerObject = new JSONObject();
                otherPlayerObject.put("player", otherPlayerDetails);

                jsonArray.add(otherPlayerObject);
            }
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(tempFilePath))) {
            writer.write(jsonArray.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            File originalFile = new File(getClass().getResource(filePath).toURI());
            File tempFile = new File(tempFilePath);
            if (originalFile.exists()) {
                Files.copy(tempFile.toPath(), originalFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                System.out.println("File updated successfully.");
            } else {
                System.out.println("Original file not found.");
            }
            tempFile.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the list of profiles.
     *
     * @return the list of profiles
     */
    public ArrayList<Profile> getProfiles() {
        return profiles;
    }
    /**
     * Returns the current player.
     *
     * @return the current player
     */
    public static Profile getCurrentPlayer() {
        return currentPlayer;
    }
    /**
     * Sets the current player.
     *
     * @param currentPlayer the current player to set
     */
    public static void setCurrentPlayer(Profile currentPlayer) {
        ProfileManager.currentPlayer = currentPlayer;
    }
    /**
     * Returns the profile with the specified name.
     *
     * @param name the name of the profile to find
     * @return the profile with the specified name, or null if not found
     */
    public Profile getProfileByName(String name) {
        for (Profile profile : profiles) {
            if (profile.getProfileName().equalsIgnoreCase(name)) {
                return profile;
            }
        }
        return null;
    }

    public  void removeProductByName(String productName) {
        // Iterate over profiles to find the current player
        for (Profile profile : profiles) {
            if (profile == currentPlayer) {
                // Iterate over goods to find the product by name
                ArrayList<String> goods = profile.getGoods();
                for (String good : goods) {
                    if (good.equalsIgnoreCase(productName)) {
                        goods.remove(good); // Remove the product
                        break;
                    }
                }
                break;
            }
        }

        // Update the JSON file after removing the product
        JSONArray jsonArray = new JSONArray();

        // Rebuild the JSON array after removing the product
        for (Profile profile : profiles) {
            JSONObject playerDetails = new JSONObject();
            playerDetails.put("name", profile.getProfileName());
            playerDetails.put("coins", profile.getCoins());
            playerDetails.put("goods", profile.getGoods());

            JSONObject playerObject = new JSONObject();
            playerObject.put("player", playerDetails);

            jsonArray.add(playerObject);
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(tempFilePath))) {
            writer.write(jsonArray.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            File originalFile = new File(getClass().getResource(filePath).toURI());
            File tempFile = new File(tempFilePath);
            if (originalFile.exists()) {
                Files.copy(tempFile.toPath(), originalFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                System.out.println("File updated successfully.");
            } else {
                System.out.println("Original file not found.");
            }
            tempFile.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
