package Profile;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The Logger class is responsible for logging events and writing them to a log file.
 */
public class Logger {

    private static final String LOG_FILE = "src\\main\\resources\\logger\\log.txt";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static Logger instance;
    private PrintWriter writer;

    /**
     * Constructs a new Logger instance and initializes the log file writer.
     */
    private  Logger() {
        try {
            writer = new PrintWriter(new FileWriter(LOG_FILE, true));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the singleton instance of the Logger class.
     *
     * @return the Logger instance
     */
    public static Logger getInstance() {
        if (instance == null) {
            instance = new Logger();
        }
        return instance;
    }

    /**
     * Logs a coin pickup event with the specified coordinates.
     *
     * @param x the x-coordinate of the coin pickup
     * @param y the y-coordinate of the coin pickup
     */
    public void logCoinPickup(double x, double y) {
        String timestamp = DATE_FORMAT.format(new Date());
        String logMessage = String.format("[%s] Coin picked up at (%.2f, %.2f)", timestamp, x, y);
        writeLog(logMessage);
        close();
    }

    /**
     * Logs the player's position with the specified coordinates.
     *
     * @param x the x-coordinate of the player's position
     * @param y the y-coordinate of the player's position
     */
    public void logPlayerPosition(double x, double y) {
        String timestamp = DATE_FORMAT.format(new Date());
        String logMessage = String.format("[%s] Player position: (%.2f, %.2f)", timestamp, x, y);
        writeLog(logMessage);
        close();
    }

    private synchronized void writeLog(String logMessage) {
        writer.println(logMessage);
        writer.flush();
    }

    /**
     * Closes the log file writer.
     */
    public void close() {
        writer.close();
    }
}