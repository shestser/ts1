package Profile;

import java.util.ArrayList;

/**
 * Represents a profile in the game.
 */
public class Profile {

    private final String profileName;
    private Long coins;
    private final ArrayList<String> goods;


    /**
     * Constructs a new profile with the given name.
     *
     * @param name the name of the profile
     */

    public Profile(String name) {
        this.profileName = name;
        this.coins = 0L;
        this.goods = new ArrayList<>();
    }

    /**
     * Constructs a new profile with the given name, coins, goods, and best scores.
     *
     * @param name   the name of the profile
     * @param coins  the amount of coins in the profile
     * @param goods  the list of goods associated with the profile

     */
    public Profile(String name, Long coins, ArrayList<String> goods) {
        this.profileName = name;        this.coins = coins;
        this.goods = goods;
    }

    /**
     * Returns the name of the profile.
     *
     * @return the name of the profile
     */
    public String getProfileName() {
        return profileName;
    }

    /**
     * Returns the amount of coins in the profile.
     *
     */
    public Long getCoins() {
        return coins;
    }

    /**
     * Sets the amount of coins in the profile.
     *
     * @param coins the amount of coins to set
     */
    public void setCoins(Long coins) {
        this.coins = coins;
    }

    /**
     * Returns the list of goods associated with the profile.
     *
     */
    public ArrayList<String> getGoods() {
        return goods;
    }

    /**
     * Returns the list of best scores associated with the profile.
     *
     */
}
