package View;

import Controller.GameStateController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;


/**
 * The GamePanel class represents the panel where the game is rendered and user input is handled.
 * It extends JPanel and implements KeyListener.
 */

public class GamePanel extends JPanel implements KeyListener {
    private GameStateController gsc;

    // Dimensions
    public static final int WIDTH = 320;
    public static final int HEIGHT = 240;
    public static final int SCALE = 2;

    // Image
    private BufferedImage image;
    private Graphics2D graphics;

    /**
     * Constructs a GamePanel object.
     * Sets up the panel dimensions, initializes the image and graphics context, and adds the key listener.
     */
    public GamePanel() {
        super();
        setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
        setFocusable(true);
        requestFocus();
        init();
    }

    /**
     * Initializes the image and graphics context of the panel.
     */
    private void init() {
        image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        graphics = (Graphics2D) image.getGraphics();
        addKeyListener(this);
    }

    /**
     * Draws the image onto the panel.
     */
    public void draw() {
        getGraphics().drawImage(image, 0, 0, WIDTH * SCALE, HEIGHT * SCALE, null);
        getGraphics().dispose();
    }

    /**
     * Returns the graphics context of the game panel.
     *
     * @return The graphics context.
     */
    public Graphics2D getGamePanelGraphics() {
        return graphics;
    }

    /**
     * Sets the GameStateController for handling game state changes.
     *
     * @param g The GameStateController.
     */
    public void setGameStateController(GameStateController g) {
        this.gsc = g;
    }

    // KeyListener methods
    public void keyTyped(KeyEvent key) {
    }

    public void keyPressed(KeyEvent key) {
        gsc.keyPressed(key.getKeyCode());
    }

    public void keyReleased(KeyEvent key) {
        gsc.keyReleased(key.getKeyCode());
    }
}