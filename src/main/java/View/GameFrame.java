package View;

import javax.swing.JFrame;

/**
 * The GameFrame class represents the main game window.
 * It creates and manages the JFrame and GamePanel components.
 */
public class GameFrame {
    private final GamePanel gamePanel;

    /**
     * Constructs a GameFrame object.
     * Initializes the game window, sets up the game panel, and makes the window visible.
     */
    public GameFrame() {
        JFrame gameWindow = new JFrame("Olympic Games");
        gamePanel = new GamePanel();

        // Set the game panel as the content pane of the game window
        gameWindow.setContentPane(gamePanel);
        gameWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gameWindow.setResizable(false);
        gameWindow.pack();
        gameWindow.setVisible(true);
    }

    /**
     * Returns the GamePanel instance associated with the GameFrame.
     *
     * @return The GamePanel instance.
     */
    public GamePanel getGamePanel() {
        return gamePanel;
    }
}