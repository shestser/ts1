package Game;

import Controller.GameStateController;
import View.GameFrame;
import View.GamePanel;

/**
 * The game engine class that controls the game loop and manages the game's lifecycle.
 * It implements the Runnable interface to support running the game in a separate thread.
 */
public class GameEngine implements Runnable {

    private GamePanel gamePanel;

    // Game Thread
    private boolean running;

    GameStateController gsc;

    /**
     * The main game loop that updates and renders the game.
     * This method is executed continuously while the game is running.
     * It updates the game state, renders the game on the screen, and manages the game's timing.
     */
    @Override
    public void run() {
        init();

        long start;
        long elapsed;
        long wait;

        // Game loop
        while (running) {
            start = System.nanoTime();
                update();
                render();

                elapsed = System.nanoTime() - start;

            int FPS = 60;
            long targetTime = 1000 / FPS;
            wait = targetTime - elapsed / 1000000;
                if (wait < 0) wait = 5;

                try {
                    Thread.sleep(wait);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    /**
     * Initializes the game engine and its components.
     * This method is called once before starting the game loop.
     * It creates the game frame, game panel, and initializes the game state controller.
     */
    private void init() {
        // Views
        // View
        GameFrame gameFrame = new GameFrame();
        gamePanel = gameFrame.getGamePanel();

        // Init GameStateController
        gsc = new GameStateController();
        gsc.setGamePanel(gamePanel);
        gamePanel.setGameStateController(gsc);

        // Init Thread
        running = true;
    }

    /**
     * Updates the game state.
     * This method is called once per frame to update the game's logic and state.
     * It delegates the update operation to the game state controller.
     */
    private void update() {
        gsc.update();
    }

    /**
     * Renders the game on the screen.
     * This method is called once per frame to render the game's graphics.
     * It delegates the rendering operation to the game state controller.
     */
    private void render() {
        gsc.render(gamePanel.getGamePanelGraphics());
    }


}