package Game;

/**
 * The main entry point of the game.
 * This class initializes the game engine, starts the game thread, and controls the game's lifecycle.
 *  It creates a new instance of the game engine, starts a new thread for the game engine, and begins the game.
 */
public class Game {
    public static void main(String... args) {
        GameEngine gameEngine = new GameEngine();
        Thread gameThread = new Thread(gameEngine);
        gameThread.start();
    }
}
