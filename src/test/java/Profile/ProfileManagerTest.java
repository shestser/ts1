package Profile;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;

/**
 * The ProfileManagerTest class is responsible for testing the functionality of the ProfileManager class.
 * It contains test cases for creating a new profile, removing a profile by name, and adding a good to the current player.
 */
public class ProfileManagerTest {

    private ProfileManager profileManager;

    /**
     * Sets up the necessary resources and initializes the ProfileManager object before each test case.
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        profileManager = ProfileManager.getInstance();
        profileManager.loadProfiles();
    }

    /**
     * Tests the creation of a new profile.
     *
     */
    @Test
    public void testCreateNewProfile() {
        // Arrange
        String profileName = "USER";

        // Get the initial number of profiles
        int initialProfileCount = profileManager.getProfiles().size();

        // Act
        boolean result = profileManager.createNewProfile(profileName);

        // Assert
        assertTrue(result);
        assertEquals(initialProfileCount + 1, profileManager.getProfiles().size());
        boolean profileFound = false;
        for (Profile profile : profileManager.getProfiles()) {
            if (profile.getProfileName().equals(profileName)) {
                profileFound = true;
                break;
            }
        }
        assertTrue(profileFound);

        profileManager.removeProfileByName(profileName);

        // Verify that the profile is removed
        Profile removedProfile = profileManager.getProfileByName(profileName);
        assertNull(removedProfile);
    }

    /**
     * Tests the removal of a profile by name.
     *
     */
    @Test
    public void testRemoveProfileByName() {
        // Arrange
        String profileName = "TEST1";
        profileManager.loadProfiles();
        profileManager.createNewProfile(profileName);

        // Get the initial number of profiles
        int initialProfileCount = profileManager.getProfiles().size();

        // Act
        profileManager.removeProfileByName(profileName);

        // Assert
        assertEquals(initialProfileCount - 1, profileManager.getProfiles().size());
        boolean profileFound = false;
        for (Profile profile : profileManager.getProfiles()) {
            if (profile.getProfileName().equals(profileName)) {
                profileFound = true;
                break;
            }
        }
        assertFalse(profileFound);
    }

    /**
     * Tests the addition of a good to the current player.
     */
    @Test
    public void testAddGoodToCurrentPlayer() {
        // Create a new profile
        profileManager =ProfileManager.getInstance();
        profileManager.loadProfiles();
        String playerName = "USER";
        boolean created = profileManager.createNewProfile(playerName);
        assertTrue(created);

        // Set the current player
        profileManager.setCurrentPlayerByName(playerName);

        // Add a good to the current player
        String goodName = "ITEM";
        profileManager.addGoodToCurrentPlayer(goodName);

        // Get the current player and check if the good is added
        Profile currentPlayer = ProfileManager.getCurrentPlayer();
        Assert.assertNotNull(currentPlayer);
        assertTrue(currentPlayer.getGoods().contains(goodName));
        profileManager.removeProfileByName(playerName);
    }






}




