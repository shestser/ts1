package States;

import Controller.GameStateController;
import Profile.Profile;
import Profile.ProfileManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StoreStateTest {
    //Testujem tridu ekvivalence do 150, user ma 149, zbozi stoji 150.
    @Test
    public void Test1() {
        ProfileManager profileManager = ProfileManager.getInstance();
        profileManager.loadProfiles();
        Profile currentProfile = profileManager.getProfileByName("TESTA");

        // Set current profile and current choice
        ProfileManager.setCurrentPlayer(currentProfile);
        StoreState storeState = new StoreState(null);

        // Get initial goods count
        int initialGoodsCount = currentProfile.getGoods().size();

        // Call select method
        GameStateController gsc = new GameStateController();
        storeState.select();
        storeState.currentChoice = 0;

        // Get updated goods count
        int updatedGoodsCount = currentProfile.getGoods().size();

        // Assert that the goods count increased by 1 after the purchase
        Assertions.assertEquals(initialGoodsCount, updatedGoodsCount);

        // Assert that the purchased item is "SHOES"
//        ArrayList<String> goods = currentProfile.getGoods();
//        Assertions.assertTrue(goods.contains("SHOES"));

    }


    //Testujem tridu ekvivalence  150-299, user ma 299, zbozi stoji 300.
    @Test
    public void Test2() {
        ProfileManager profileManager = ProfileManager.getInstance();
        profileManager.loadProfiles();
        Profile currentProfile = profileManager.getProfileByName("TESTC");

        // Set current profile and current choice
        ProfileManager.setCurrentPlayer(currentProfile);
        StoreState storeState = new StoreState(null);

        // Get initial goods count
        int initialGoodsCount = currentProfile.getGoods().size();

        // Call select method
        GameStateController gsc = new GameStateController();
        storeState.select();
        storeState.currentChoice = 2;   // snazime se koupit doping za 300

        // Get updated goods count
        int updatedGoodsCount = currentProfile.getGoods().size();

        // Assert that the goods count increased by 1 after the purchase
        Assertions.assertEquals(initialGoodsCount, updatedGoodsCount);


        int updatedGoodsCount2 = currentProfile.getGoods().size();
        gsc =new GameStateController();
        storeState.select();
        storeState.currentChoice = 1;   // snazime se koupit shoes za 150

        // Get updated goods count

        // Assert that the goods count increased by 1 after the purchase
        Assertions.assertEquals(initialGoodsCount+1, updatedGoodsCount2);



    }


}