package States;

import Controller.GameStateController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

class MenuStateTest {

    private GameStateController gameStateController;
    private MenuState menuState;
    private Graphics2D graphics;

    @BeforeEach
    void setUp() {
        gameStateController = mock(GameStateController.class);
        menuState = new MenuState(gameStateController);
        graphics = mock(Graphics2D.class);
    }

    @Test
    void select_StartOption_ChangeGameStateToChooseLevelState() {
        menuState.currentChoice = 0;

        menuState.select();

        verify(gameStateController).setState(GameStateController.getChooseLevelState());
    }

    @Test
    void select_StoreOption_ChangeGameStateToStoreState() {
        menuState.currentChoice = 1;

        menuState.select();

        verify(gameStateController).setState(GameStateController.getStoreState());
    }


    @Test
    void render_CurrentChoice_SetColorToRed() {
        menuState.currentChoice = 1;

        menuState.render(graphics);

        verify(graphics).setColor(Color.RED);
    }

    @Test
    void keyPressed_DownKey_WrapAroundToFirstChoice() {
        KeyEvent keyEvent = mock(KeyEvent.class);
        when(keyEvent.getKeyCode()).thenReturn(KeyEvent.VK_DOWN);

        menuState.currentChoice = menuState.options.length - 1;

        menuState.keyPressed(keyEvent.getKeyCode());

        Assertions.assertEquals(0, menuState.currentChoice);
    }



        @Test
        public void testFilePath() {
            MenuState menuState = new MenuState(null);
            String filePath = menuState.getClass().getResource("/images/Backgrounds/menubg.gif").getPath();
            File file = new File(filePath);
            assertTrue(file.exists());
        }
    }



