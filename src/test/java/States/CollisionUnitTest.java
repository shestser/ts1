package States;

import Entities.Obstacle;
import Entities.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CollisionUnitTest {

        private LevelRunningState levelRunningState;

        @BeforeEach
        void setUp() {
            levelRunningState = new LevelRunningState(null);
        }

        @Test
        void checkPlayerCollisionWithObstacles() {
            Obstacle obstacle = new Obstacle(null);
            Player player = new Player(null);

            obstacle.setPosition(100, 200);
            player.setPosition(100, 200);

            assertFalse(player.intersects(obstacle));

            levelRunningState.update();

            assertTrue(player.intersects(obstacle));
        }
    }
