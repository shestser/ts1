package States;

import Controller.GameStateController;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class LevelRunningStateTest {

    @Test
    void initTest() {
        // Create an instance of GameStateController (gsc)
        GameStateController gsc = new GameStateController();

        // Create an instance of LevelRunningState
        LevelRunningState levelRunningState = new LevelRunningState(gsc);

        // Call the init method
        levelRunningState.init();

        // Check that the tile map is initialized
        assertNotNull(levelRunningState.getTileMap());

        // Check that the background is initialized
        assertNotNull(levelRunningState.getBg());

        // Check that the player is initialized
        assertNotNull(levelRunningState.getPlayer());

        // Check that the obstacles list is initialized and contains the correct number of obstacles
        assertNotNull(levelRunningState.getObstacles());
        assertEquals(3, levelRunningState.getObstacles().size());

        // Check that the coins list is initialized and contains the correct number of coins
        assertNotNull(levelRunningState.getCoins());
        assertEquals(4, levelRunningState.getCoins().size());

    }
}