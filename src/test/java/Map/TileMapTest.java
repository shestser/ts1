package Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.awt.image.BufferedImage;

public class TileMapTest {

    @Test
    public void testDraw() {
        TileMap tileMap = new TileMap(32);
        tileMap.loadTiles("/images/Map/run.png");
        tileMap.loadMap("/images/Map/runninglevel.map");

        BufferedImage image = new BufferedImage(800, 600, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = image.createGraphics();

        // Call the draw method of TileMap
        tileMap.draw(g);
        int pixelColor = image.getRGB(100, 100); // Check the color at position (100, 100)
        int expectedColor = 0xFF0000FF; // Assuming that the tile at (100, 100) should be blue
        Assertions.assertEquals(expectedColor, pixelColor, "Map not rendered correctly at (100, 100)");
    }
}