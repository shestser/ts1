package Controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GameStateControllerTest {

    private GameStateController gameStateController;

    @BeforeEach
    public void setUp() {
        gameStateController = new GameStateController();
    }
    @Test
    public void testStateTransition() {
        assertEquals(GameStateController.getProfileState(), gameStateController.getCurrentState());

        gameStateController.setState(GameStateController.getMenuState());
        assertEquals(GameStateController.getMenuState(), gameStateController.getCurrentState());

        gameStateController.setState(GameStateController.getChooseLevelState());
        assertEquals(GameStateController.getChooseLevelState(), gameStateController.getCurrentState());

        gameStateController.setState(GameStateController.getLevelRunningState());
        assertEquals(GameStateController.getLevelRunningState(), gameStateController.getCurrentState());

        gameStateController.setState(GameStateController.getGameOverState());
        assertEquals(GameStateController.getGameOverState(), gameStateController.getCurrentState());

        gameStateController.setState(GameStateController.getStoreState());
        assertEquals(GameStateController.getStoreState(), gameStateController.getCurrentState());

        gameStateController.setState(GameStateController.getProfileState());
        assertEquals(GameStateController.getProfileState(), gameStateController.getCurrentState());

        gameStateController.setState(GameStateController.getNewProfileState());
        assertEquals(GameStateController.getNewProfileState(), gameStateController.getCurrentState());
    }
}